import pcbnew
import os
import wx

successMsg = "DRC rules updated, you can check them in Board Setup -> Custom rules"

def Kicad_string( name, data ):
    result=''
    for key in data:
        result += '(rule "IsoMatrix_'+name+'_'+str(key)+'"'
        #result += '\n'
        result += '(constraint clearance (min '+str(key)+'mm))'
        result += '(condition "A.NetClass == \''+name+'\' && ('
        for netname in data[key]:
            result += ' B.NetClass == \''+netname+'\' ||'
        result = result[:-2]
        result += ')'
        #If not PD1
        # -- Conformal coating
        result += ' && ( ! A.enclosedByArea(\'PD1\') ) && ( ! A.enclosedByArea(\'PD1\') ) '
        # -- Internal layer

        result += '"))'
    return result

def convertExcelFile( aFilepath ):

    if ( not os.path.exists( aFilepath ) ):
        raise FileNotFoundError

    exl = pd.read_excel( aFilepath, sheet_name='FDistExt')

    i= 1
    ListOfNames = []

    while ( exl.iloc[1,i] != 0 ):
        ListOfNames.append( exl.iloc[1,i] )
        i = i+1

    matrix = {}

    for i in range( len(ListOfNames) ):
        matrix[ListOfNames[i]] ={}
        for j in range(i):
            matrix[ListOfNames[i]][ListOfNames[j]] = exl.iloc[i+2,j+1]

    result = ""
    for key1 in matrix:
        d = {}
        for key2, value in matrix[key1].items():
            if value not in d:
                d[value] = [key2]
            else:
                d[value].append(key2)
        if (key1 != ''):
            result += Kicad_string(key1, d) + '\n'
    return result


class SimplePlugin(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "Isolation Matrix"
        self.category = "tools"
        self.description = "Creates isolation rules for all netclasses"
        self.show_toolbar_button = False  # Optional, defaults to False
        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'simple_plugin.png')  # Optional, defaults to ""

    def Run(self):

        import pandas as pd
        import numpy as np
        # Get the current board
        board = pcbnew.GetBoard()
        if not board:
            wx.MessageBox("No board loaded!", "Error", wx.ICON_ERROR)
            return

        # Construct the path for the .dru file
        path = board.GetFileName()
        path = path[:-3] + 'dru'

        begin_token = "#KisoMatrix BEGIN"
        end_token = "#KisoMatrix END"

        matrceKisoFilename = os.path.dirname( (board.GetFileName() ))+'/matricekiso.xls'
        try:
            additional_content = convertExcelFile( matrceKisoFilename )
        except FileNotFoundError:
            wx.MessageBox(
                f"File does not exist: {matrceKisoFilename} \n (Place a matrice iso file into the project folder and rename it to the target name)",
                "Error", wx.ICON_ERROR)
            return

        try:
            # Try to open the .dru file in read/write mode
            with open(path, "r+") as f:
                content = f.read()
                print(content)  # Example operation: print the file content

                # Check if the begin token is in the file
                begin_index = content.find(begin_token)
                if begin_index != -1:
                    end_index = content.find(end_token, begin_index)
                    if end_index != -1:
                        # Delete everything between begin and end tokens
                        content = content[:begin_index + len(begin_token)] + f"\n{additional_content}\n" + content[end_index:]
                    else:
                        # Add the end token after the begin token
                        content = content[:begin_index + len(begin_token)] + f"\n{additional_content}\n{end_token}\n" + content[begin_index + len(begin_token):]

                    # Move the file pointer to the beginning and write the modified content
                    f.seek(0)
                    f.write(content)
                    f.truncate()

                    wx.MessageBox(successMsg, "Info", wx.ICON_INFORMATION)

                else:
                    # Append the begin token, TEST, and end token to the file
                    f.write(f"\n{begin_token}\n{additional_content}\n{end_token}\n")
                    wx.MessageBox( successMsg, "Info", wx.ICON_INFORMATION)

        except FileNotFoundError:
            # If the file is not found, create it and write initial content
            with open(path, "w") as f:
                initial_content = f"(version 1)\n\n{begin_token}\n{additional_content}\n{end_token}\n"
                f.write(initial_content)
                wx.MessageBox(successMsg, "Info", wx.ICON_INFORMATION)

# Instantiate and register the plugin
SimplePlugin().register()
